#!/bin/sh
# https://codecalamity.com/a-raspberry-pi-streaming-camera-using-mpeg-dash-hls-or-rtsp/

DIR=/dev/shm/streaming

set -xe

mkdir -p $DIR
ln -fs $DIR
ln -frs index.html streaming/

trap "pkill -e -P $$" EXIT

# https://ffmpeg.org/ffmpeg-formats.html#dash-2
ffmpeg -framerate 30 -i /dev/video4 -c:v libx264 -preset ultrafast -an -f dash -streaming 1 -ldash 1 -utc_timing_url https://time.akamai.com/?iso -window_size 10 -remove_at_exit 1 $DIR/manifest.mpd &

python -m http.server -d streaming
